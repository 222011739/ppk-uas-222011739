import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vixy/Utilities/constrant.dart';
import 'package:vixy/Pages/login_page.dart';
import 'package:vixy/Pages/EditProfile_page.dart';
import 'package:vixy/Pages/pesan_page.dart';
import 'package:vixy/Pages/progress_page.dart';
import 'package:vixy/Pages/profile_page.dart';
import 'package:video_player/video_player.dart';

class homePage extends StatefulWidget {
  const homePage({super.key});

  @override
  State<homePage> createState() => _homePage();
}

class _homePage extends State<homePage> {
  late VideoPlayerController _video1; //, _video2, _video3;
  late Future<void> _initVideo1; //, _initVideo2, _initVideo3;

  @override
  void initState() {
    _video1 = VideoPlayerController.asset('assets/videos/BumperLogo.m4v');
    // _video2 = VideoPlayerController.asset('assets/videos/Teaser.m4v');
    // _video3 = VideoPlayerController.asset('assets/videos/AfterMovie.m4v');

    _initVideo1 = _video1.initialize();
    _video1.setLooping(true);
    _video1.play();
    _video1.setVolume(0);

    // _initVideo2 = _video2.initialize();
    // _video2.setLooping(true);
    // _video2.play();
    // _video2.setVolume(0);

    // _initVideo3 = _video3.initialize();
    // _video3.setLooping(true);
    // _video3.play();
    // _video3.setVolume(0);

    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    _video1.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 18, 18, 18),
      appBar: AppBar(
        title: Image(
          image: Image.asset(logoDark).image,
          width: 130,
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: secondaryColor),
        backgroundColor: mainColor,
      ),
      drawer: Drawer(
        backgroundColor: mainColor,
        child: Container(
          decoration: BoxDecoration(
            color: mainColor,
          ),
          child: ListView(
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  color: secondaryColor,
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color.fromARGB(255, 18, 18, 18),
                      secondaryColor,
                    ],
                  ),
                ),
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Image.asset(
                    logo,
                    width: 200,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text("Home"),
                onTap: () {
                  Fluttertoast.showToast(
                      msg: "Anda sudah berada di halaman Home",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      backgroundColor: Color.fromARGB(255, 50, 43, 9),
                      textColor: Colors.white,
                      fontSize: 16.0);
                },
              ),
              ListTile(
                leading: Icon(Icons.person),
                title: Text("Profile"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => profilePage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.account_balance_wallet_rounded),
                title: Text("Pesan"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => pesanPage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.search),
                title: Text("Cek Progress Pesanan"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => progressPage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.logout),
                title: Text("Logout"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => loginPage()));
                },
              ),
            ],
          ),
        ),
      ),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          padding: EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 70,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Ciptakan momen indahmu dan buatlah kenangan dengan vixy",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 100),
              Text(
                "Aplikasi ini adalah aplikasi yang tepat untuk anda yang membutuhkan jasa editing video yang terpercaya, berpengalaman, kreatif, dan mengikuti perkembangan jaman",
                style: TextStyle(
                  color: mainColor,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 100),
              Text(
                "Some of our products",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              ),
              SizedBox(height: 20),
              AspectRatio(
                aspectRatio: 16 / 9,
                child: VideoPlayer(
                  _video1,
                ),
              ),
              SizedBox(height: 20),
              // AspectRatio(
              //   aspectRatio: 16 / 9,
              //   child: VideoPlayer(
              //     _video2,
              //   ),
              // ),
              // SizedBox(height: 20),
              // AspectRatio(
              //   aspectRatio: 16 / 9,
              //   child: VideoPlayer(
              //     _video3,
              //   ),
              // ),
              // SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
