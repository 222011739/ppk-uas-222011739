import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vixy/Pages/profile_page.dart';
import 'package:vixy/Pages/progress_page.dart';

import '../Utilities/constrant.dart';
import 'landing_page.dart';
import 'login_page.dart';

class pesanPage extends StatefulWidget {
  const pesanPage({super.key});

  @override
  State<pesanPage> createState() => _pesanPageState();
}

class _pesanPageState extends State<pesanPage> {
  Widget _buildPaketDd() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Paket',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        DropdownButton(
            items: [
              DropdownMenuItem(
                child: Text("Paket 1"),
                value: 1,
              ),
              DropdownMenuItem(
                child: Text("Paket 2"),
                value: 2,
              ),
              DropdownMenuItem(
                child: Text("Paket 3"),
                value: 3,
              ),
            ],
            onChanged: (value) {
              print(value);
            })
      ],
    );
  }

  Widget _buildPesanBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25),
      width: 150,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 5.0,
          padding: EdgeInsets.all(15.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          primary: mainColor,
        ),
        onPressed: () {
          Fluttertoast.showToast(
              msg: "Pesanan berhasil ditambahkan",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Color.fromARGB(255, 50, 43, 9),
              textColor: Colors.white,
              fontSize: 16.0);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => profilePage()),
          );
        },
        child: Text(
          'Pesan',
          style: TextStyle(
            color: Color.fromARGB(255, 20, 19, 3),
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 18, 18, 18),
      appBar: AppBar(
        title: Text(
          "Pesan",
          style: TextStyle(color: secondaryColor, fontWeight: FontWeight.bold),
        ),
        iconTheme: IconThemeData(color: secondaryColor),
        backgroundColor: mainColor,
      ),
      drawer: Drawer(
        backgroundColor: mainColor,
        child: Container(
          decoration: BoxDecoration(
            color: mainColor,
          ),
          child: ListView(
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  color: secondaryColor,
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color.fromARGB(255, 18, 18, 18),
                      secondaryColor,
                    ],
                  ),
                ),
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Image.asset(
                    logo,
                    width: 200,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text("Home"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => homePage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.person),
                title: Text("Profile"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => profilePage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.account_balance_wallet_rounded),
                title: Text("Pesan"),
                onTap: () {
                  Fluttertoast.showToast(
                      msg: "Anda sudah berada di halaman pemesanan",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      backgroundColor: Color.fromARGB(255, 50, 43, 9),
                      textColor: Colors.white,
                      fontSize: 16.0);
                },
              ),
              ListTile(
                leading: Icon(Icons.search),
                title: Text("Cek Progress Pesanan"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => progressPage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.logout),
                title: Text("Logout"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => loginPage()));
                },
              ),
            ],
          ),
        ),
      ),
      body: Container(
          height: double.infinity,
          color: Color.fromARGB(255, 18, 18, 18),
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(
              horizontal: 40,
            ),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text(
                    "Pesan",
                    style: TextStyle(
                      color: mainColor,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Text(
                    "Pilih paket yang anda inginkan",
                    style: TextStyle(
                      color: mainColor,
                      fontSize: 20,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: 150,
                        height: 150,
                        decoration: BoxDecoration(
                          color: secondaryColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Paket 1",
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "- Revisi: 1x",
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              "- Waktu: 4 Hari",
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: 150,
                        height: 150,
                        decoration: BoxDecoration(
                          color: secondaryColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Paket 2",
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "- Revisi: 2x",
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              "- Waktu: 5 Hari",
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: 150,
                        height: 150,
                        decoration: BoxDecoration(
                          color: secondaryColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Paket 3",
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "- Revisi: 3x",
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              "- Waktu: 7 Hari",
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                _buildPaketDd(),
                SizedBox(
                  height: 20,
                ),
                _buildPesanBtn(),
              ],
            ),
          )),
    );
  }
}
