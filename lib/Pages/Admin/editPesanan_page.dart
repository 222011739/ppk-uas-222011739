import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vixy/DBhandler.dart';
import 'package:vixy/Pages/Admin/DaftarPesanan_page.dart';
import 'package:vixy/Pages/Admin/landingAdmin_page.dart';
import 'package:vixy/Pages/landing_page.dart';
import 'package:vixy/Pages/pesan_page.dart';
import 'package:vixy/Pages/profile_page.dart';
import 'package:vixy/Pages/progress_page.dart';
import 'package:vixy/Utilities/constrant.dart';
import 'package:vixy/Pages/login_page.dart';
import 'package:vixy/model.dart';

class editPesananPage extends StatefulWidget {
  const editPesananPage({super.key, required this.order});

  final Order order;

  @override
  State<editPesananPage> createState() => _editPesananPageState();
}

class _editPesananPageState extends State<editPesananPage> {
  var valuePaket, valueProgress;
  HTTPstateful http = new HTTPstateful();

  Widget _buildNamaTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Nama',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60,
          //set padding left
          child: TextField(
            enableInteractiveSelection: false,
            focusNode: new AlwaysDisabledFocusNode(),
            style: TextStyle(
              color: mainColor,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.person,
                color: mainColor,
              ),
              hintText: widget.order.nama,
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60,
          child: TextField(
            enableInteractiveSelection: false,
            focusNode: new AlwaysDisabledFocusNode(),
            keyboardType: TextInputType.phone,
            style: TextStyle(
              color: mainColor,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.email,
                color: mainColor,
              ),
              hintText: widget.order.email,
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildHPTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Nomor Handphone',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60,
          child: TextField(
            enableInteractiveSelection: false,
            focusNode: new AlwaysDisabledFocusNode(),
            keyboardType: TextInputType.phone,
            style: TextStyle(
              color: mainColor,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.phone,
                color: mainColor,
              ),
              hintText: widget.order.noHP,
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPaketTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Jenis Paket',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        DropdownButton(
          value: valuePaket,
          items: [
            DropdownMenuItem(
              child: Text("Paket 1"),
              value: "Paket 1",
            ),
            DropdownMenuItem(
              child: Text("Paket 2"),
              value: "Paket 2",
            ),
            DropdownMenuItem(
              child: Text("Paket 3"),
              value: "Paket 3",
            ),
          ],
          onChanged: (value) {
            print(value);
            setState(() {
              valuePaket = value.toString();
            });
          },
          hint: Text(
            widget.order.paket,
            style: TextStyle(
              color: mainColor,
            ),
          ),
          dropdownColor: secondaryColor,
          style: TextStyle(
            color: mainColor,
          ),
        ),
      ],
    );
  }

  Widget _buildProgressDd() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Progress',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        DropdownButton(
          value: valueProgress,
          items: [
            DropdownMenuItem(
              child: Text("Dalam Pengerjaan"),
              value: "Dalam Pengerjaan",
            ),
            DropdownMenuItem(
              child: Text("Dalam Antrian"),
              value: "Dalam Antrian",
            ),
            DropdownMenuItem(
              child: Text("Selesai"),
              value: "Selesai",
            ),
          ],
          onChanged: (value) {
            print(value);
            setState(() {
              valueProgress = value.toString();
            });
          },
          hint: Text(
            widget.order.progress,
            style: TextStyle(
              color: mainColor,
            ),
          ),
          dropdownColor: secondaryColor,
          style: TextStyle(
            color: mainColor,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 18, 18, 18),
        appBar: AppBar(
          title: Text(
            "Edit Pesanan",
            style:
                TextStyle(color: secondaryColor, fontWeight: FontWeight.bold),
          ),
          iconTheme: IconThemeData(color: secondaryColor),
          backgroundColor: mainColor,
        ),
        drawer: Drawer(
          backgroundColor: mainColor,
          child: Container(
            decoration: BoxDecoration(
              color: mainColor,
            ),
            child: ListView(
              children: [
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: secondaryColor,
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Color.fromARGB(255, 18, 18, 18),
                        secondaryColor,
                      ],
                    ),
                  ),
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Image.asset(
                      logo,
                      width: 200,
                    ),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.home),
                  title: Text("Home"),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => homePageAdmin()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.search),
                  title: Text("Daftar Pesanan"),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => daftarPesananPage()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text("Logout"),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => loginPage()));
                  },
                ),
              ],
            ),
          ),
        ),
        body: Container(
          height: double.infinity,
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 30),
                _buildNamaTF(),
                SizedBox(height: 30),
                _buildEmailTF(),
                SizedBox(height: 30),
                _buildHPTF(),
                SizedBox(height: 30),
                _buildPaketTF(),
                SizedBox(height: 30),
                _buildProgressDd(),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 25),
                  width: 150,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 5.0,
                      padding: EdgeInsets.all(15.0),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      primary: mainColor,
                    ),
                    onPressed: () async {
                      String id = widget.order.id;
                      bool response =
                          await http.updateData(id, valuePaket, valueProgress);
                      if (response) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => daftarPesananPage()));
                      } else {
                        print("gagal");
                      }
                    },
                    child: Text(
                      'Update',
                      style: TextStyle(
                        color: Color.fromARGB(255, 20, 19, 3),
                        letterSpacing: 1.5,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
