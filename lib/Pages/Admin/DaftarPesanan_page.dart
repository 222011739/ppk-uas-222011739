import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vixy/DBhandler.dart';
import 'package:vixy/Pages/Admin/editPesanan_page.dart';
import 'package:vixy/Pages/Admin/landingAdmin_page.dart';
import 'package:vixy/Pages/login_page.dart';
import 'package:vixy/Utilities/constrant.dart';
import 'package:vixy/model.dart';

class daftarPesananPage extends StatefulWidget {
  const daftarPesananPage({super.key});

  @override
  State<daftarPesananPage> createState() => _daftarPesananPageState();
}

class _daftarPesananPageState extends State<daftarPesananPage> {
  List<Order> orders = [];
  HTTPstateful http = HTTPstateful();

  getAllData() async {
    orders = await http.getAllData();
  }

  @override
  void initState() {
    // TODO: implement initState
    getAllData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 18, 18, 18),
      appBar: AppBar(
        title: Text(
          "Daftar Pesanan",
          style: TextStyle(color: secondaryColor, fontWeight: FontWeight.bold),
        ),
        iconTheme: IconThemeData(color: secondaryColor),
        backgroundColor: mainColor,
      ),
      drawer: Drawer(
        backgroundColor: mainColor,
        child: Container(
          decoration: BoxDecoration(
            color: mainColor,
          ),
          child: ListView(
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  color: secondaryColor,
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color.fromARGB(255, 18, 18, 18),
                      secondaryColor,
                    ],
                  ),
                ),
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Image.asset(
                    logo,
                    width: 200,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text("Home"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => homePageAdmin()));
                },
              ),
              ListTile(
                leading: Icon(Icons.search),
                title: Text("Daftar Pesanan"),
                onTap: () {
                  Fluttertoast.showToast(
                      msg: "Anda sudah berada di halaman Home",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      backgroundColor: Color.fromARGB(255, 50, 43, 9),
                      textColor: Colors.white,
                      fontSize: 16.0);
                },
              ),
              ListTile(
                leading: Icon(Icons.logout),
                title: Text("Logout"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => loginPage()));
                },
              ),
            ],
          ),
        ),
      ),
      body: ListView.separated(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.only(top: 15),
          itemBuilder: (context, index) {
            return ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.only(top: 15, bottom: 15),
                primary: mainColor,
                onPrimary: secondaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => editPesananPage(order: orders[index]),
                  ),
                );
              },
              onLongPress: () {
                // Delete Alert
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text("Hapus Pesanan"),
                        content: Text(
                            "Apakah anda yakin ingin menghapus pesanan ini?"),
                        actions: [
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text("Tidak"),
                          ),
                          TextButton(
                            onPressed: () async {
                              await http.deleteData(orders[index].id);
                              Navigator.pop(context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => daftarPesananPage(),
                                ),
                              );
                            },
                            child: Text("Ya"),
                          ),
                        ],
                      );
                    });
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Nama: ${orders[index].nama}",
                    style: TextStyle(
                        color: secondaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  Text(
                    "Email: ${orders[index].email}",
                    style: TextStyle(
                        color: secondaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  Text(
                    "No HP: ${orders[index].noHP}",
                    style: TextStyle(
                        color: secondaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  Text(
                    "Paket: ${orders[index].paket}",
                    style: TextStyle(
                        color: secondaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  Text(
                    "Progress: ${orders[index].progress}",
                    style: TextStyle(
                        color: secondaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                ],
              ),
            );
          },
          separatorBuilder: ((context, index) {
            return Divider(
              color: mainColor,
            );
          }),
          itemCount: orders.length),
    );
  }
}
