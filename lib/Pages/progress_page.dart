import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vixy/Pages/pesan_page.dart';
import 'package:vixy/Pages/profile_page.dart';

import '../Utilities/constrant.dart';
import 'landing_page.dart';
import 'login_page.dart';

class progressPage extends StatefulWidget {
  const progressPage({super.key});

  @override
  State<progressPage> createState() => _progressPageState();
}

class _progressPageState extends State<progressPage> {
  Widget _buildHPTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'ID Pesanan',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60,
          child: TextField(
            keyboardType: TextInputType.phone,
            style: TextStyle(
              color: mainColor,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14),
              prefixIcon: Icon(
                Icons.search,
                color: mainColor,
              ),
              hintText: 'Masukkan ID pesanan anda',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildUpdateBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25),
      width: 150,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 5.0,
          padding: EdgeInsets.all(15.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          primary: mainColor,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => profilePage()),
          );
        },
        child: Text(
          'Cek',
          style: TextStyle(
            color: Color.fromARGB(255, 20, 19, 3),
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 18, 18, 18),
      appBar: AppBar(
        title: Text(
          "Cek Progress Pesanan",
          style: TextStyle(color: secondaryColor, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: secondaryColor),
        backgroundColor: mainColor,
      ),
      drawer: Drawer(
        backgroundColor: mainColor,
        child: Container(
          decoration: BoxDecoration(
            color: mainColor,
          ),
          child: ListView(
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  color: secondaryColor,
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      Color.fromARGB(255, 18, 18, 18),
                      secondaryColor,
                    ],
                  ),
                ),
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Image.asset(
                    logo,
                    width: 200,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text("Home"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => homePage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.person),
                title: Text("Profile"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => profilePage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.account_balance_wallet_rounded),
                title: Text("Pesan"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => pesanPage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.search),
                title: Text("Cek Progress Pesanan"),
                onTap: () {
                  Fluttertoast.showToast(
                      msg: "Anda sudah berada di halaman cek progress",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      backgroundColor: Color.fromARGB(255, 50, 43, 9),
                      textColor: Colors.white,
                      fontSize: 16.0);
                },
              ),
              ListTile(
                leading: Icon(Icons.logout),
                title: Text("Logout"),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => loginPage()));
                },
              ),
            ],
          ),
        ),
      ),
      body: Container(
        height: double.infinity,
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          padding: EdgeInsets.symmetric(
            horizontal: 40,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 30),
              _buildHPTF(),
              _buildUpdateBtn(),
            ],
          ),
        ),
      ),
    );
  }
}
