import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vixy/Pages/progress_page.dart';
import 'package:vixy/Utilities/constrant.dart';
import 'package:vixy/Pages/login_page.dart';
import 'package:vixy/Pages/editProfile_page.dart';
import 'package:vixy/Pages/pesan_page.dart';

import 'landing_page.dart';

class profilePage extends StatefulWidget {
  const profilePage({super.key});

  @override
  State<profilePage> createState() => _profilePage();
}

class _profilePage extends State<profilePage> {
  Widget _buildNamaTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Nama',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60,
          //set padding left
          padding: EdgeInsets.only(left: 15),
          child: Text(
            'Muhammad Naufal Faishal',
            style: TextStyle(
              color: mainColor,
              fontFamily: 'OpenSans',
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60,
          padding: EdgeInsets.only(left: 15),
          child: Text(
            'naufal1faishal@gmail.com',
            style: TextStyle(
              color: mainColor,
              fontFamily: 'OpenSans',
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildHPTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Nomor Handphone',
          style: kLabelStyle,
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60,
          padding: EdgeInsets.only(left: 15),
          child: Text(
            '081917558751',
            style: TextStyle(
              color: mainColor,
              fontFamily: 'OpenSans',
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildEditBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25),
      width: 150,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 5.0,
          padding: EdgeInsets.all(15.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          primary: mainColor,
        ),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => editProfilePage()));
        },
        child: Text(
          'Edit',
          style: TextStyle(
            color: Color.fromARGB(255, 20, 19, 3),
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 18, 18, 18),
        appBar: AppBar(
          title: Text(
            "Profile",
            style:
                TextStyle(color: secondaryColor, fontWeight: FontWeight.bold),
          ),
          iconTheme: IconThemeData(color: secondaryColor),
          backgroundColor: mainColor,
        ),
        drawer: Drawer(
          backgroundColor: mainColor,
          child: Container(
            decoration: BoxDecoration(
              color: mainColor,
            ),
            child: ListView(
              children: [
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: secondaryColor,
                    gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        Color.fromARGB(255, 18, 18, 18),
                        secondaryColor,
                      ],
                    ),
                  ),
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Image.asset(
                      logo,
                      width: 200,
                    ),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.home),
                  title: Text("Home"),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => homePage()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text("Profile"),
                  onTap: () {
                    Fluttertoast.showToast(
                        msg: "Anda sudah berada di halaman Profile",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        backgroundColor: Color.fromARGB(255, 50, 43, 9),
                        textColor: Colors.white,
                        fontSize: 16.0);
                  },
                ),
                ListTile(
                  leading: Icon(Icons.account_balance_wallet_rounded),
                  title: Text("Pesan"),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => pesanPage()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.search),
                  title: Text("Cek Progress Pesanan"),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => progressPage()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text("Logout"),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => loginPage()));
                  },
                ),
              ],
            ),
          ),
        ),
        body: Container(
          height: double.infinity,
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(
              horizontal: 20,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 30),
                _buildNamaTF(),
                SizedBox(height: 30),
                _buildEmailTF(),
                SizedBox(height: 30),
                _buildHPTF(),
                _buildEditBtn(),
              ],
            ),
          ),
        ));
  }
}
