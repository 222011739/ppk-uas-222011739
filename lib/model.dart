import 'dart:convert';

import 'package:http/http.dart' as http;

class Order {
  // final int id, noHP;
  String id, noHP, nama, email, paket, progress;

  Order({
    required this.id,
    required this.nama,
    required this.email,
    required this.noHP,
    required this.paket,
    required this.progress,
  });

  @override
  String toString() {
    return '{ ${this.id}, ${this.nama}, ${this.email}, ${this.noHP}, ${this.paket}, ${this.progress} }';
  }

  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
      id: json['id'],
      nama: json['Nama'],
      email: json['Email'],
      noHP: json['no_handphone'],
      paket: json['Paket'],
      progress: json['Progress'],
    );
  }
}
