import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:vixy/Pages/Admin/landingAdmin_page.dart';
import 'package:vixy/Pages/login_page.dart';
import 'package:vixy/Pages/profile_page.dart';
import 'package:vixy/Pages/editProfile_page.dart';
import 'package:vixy/Pages/landing_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Vixy",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: homePageAdmin(),
    );
  }
}
