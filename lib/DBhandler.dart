// HTTP package
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:vixy/model.dart';

const String baseUrl = 'http://10.0.2.2:8080';

class HTTPstateful {
  // String id, name, email, password;

  final _baseUrl = 'https://222011739.student.stis.ac.id/';

  // HTTPstateful({this.id, this.name, this.email, this.password});

  var Client = http.Client();

  Future<dynamic> getAllData() async {
    var url = Uri.parse(_baseUrl + "/orders");
    var response = await http.get(url);
    if (response.statusCode == 200) {
      Iterable list = jsonDecode(response.body);
      // var output = response.body;
      // Map<String, dynamic> map = jsonDecode(response.body);
      // List list = map["orders"];
      // var cek = list.map((e) => Order.fromJson(e));
      List<Order> orders =
          List<Order>.from(list.map((model) => Order.fromJson(model)).toList());
      return orders;
    } else {
      print("error");
    }
  }

  // Delete Data
  Future<dynamic> deleteData(String id) async {
    var url = Uri.parse(_baseUrl + "/orders/" + id);
    var response = await http.delete(url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print("error");
    }
  }

  // Update Data
  Future<dynamic> updateData(String id, String paket, String progress) async {
    try {
      var url = Uri.parse(_baseUrl + "/orders/" + id);
      final response = await http.put(url,
          headers: {"Content-Type": "application/json"},
          body: {"Paket": paket, "Progress": progress});
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e);
    }
  }

  // Create Data
  Future<dynamic> createData(String nama, String email, String noHP,
      String paket, String progress) async {
    var url = Uri.parse(_baseUrl + "/orders");
    var response = await http.post(url,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode({
          "Nama": nama,
          "Email": email,
          "no_handphone": noHP,
          "Paket": paket,
          "Progress": progress
        }));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print("error");
    }
  }
}
