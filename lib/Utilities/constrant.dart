import 'package:flutter/material.dart';

// COLOR
final mainColor = Color.fromARGB(255, 246, 223, 184);
final secondaryColor = Color.fromARGB(255, 37, 32, 7);

// LOGO
final logo = "assets/images/Logo.png";
final logoDark = "assets/images/Logo_dark.png";

// TEXT STYLE
final kHintTextStyle = TextStyle(
  color: Colors.white54,
  fontFamily: 'OpenSans',
);

final kLabelStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

// BOX DECORATION
final kBoxDecorationStyle = BoxDecoration(
  color: Color.fromARGB(255, 37, 32, 7),
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
